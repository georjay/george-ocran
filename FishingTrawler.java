/**
 * 
 * @author George Ocran
 */

public class FishingTrawler{
  
  double[] compart = new double[100];
  int compartmentNumb = 0; // variable to go through compartments
  int numCatch = 0;
  double totalWeight = 0;
  public static final int NUM_CATCHES = 100;
  public static final int MAX_WEIGHT = 1000;
  
  /**
   * function to input weight of new catch.
   * Goes through compartments chronologically and inputs
   * check to make sure total number of catches and total weight are not exceeded
   * @param take in a weight in decimal
   * no return value
   */
  public void inputCatch(double t){
    if (maxCatch() == false && totalWeight())
      compart[compartmentNumb] = t;
    compartmentNumb++;
    numCatch++;
    totalWeight += t;
  }
  
  /**
   * function to check if all maximum catches reached
   * returns true or false
   */
  public boolean maxCatch(){
    boolean it = false;
    if(numCatch < NUM_CATCHES){
      it = true;
    }
    else
      it = false;
    return it;
  }
  
  /**
   * function to check if maximum weight is reached
   * returns true or false
   */
  public boolean totalWeight(){
    boolean it = false;
    if(totalWeight < MAX_WEIGHT){
      it = true; 
    }
    else
      it = false;
    return it;
  }
  
  /**
   * function to return to total weight of all catches
   * returns double
   */
  public double totalCatchWeight(){
    return totalWeight;
  }
  
  /**
   * function to return the total number of catches
   * returns an int
   */
  public int totalNumCatch(){
   return numCatch;
  }
  
  /**
   * function to return the average weight
   * returns double
   */
  public double avgWeight(){
   return (totalWeight / numCatch); 
  }
}